# [bash](https://pkgs.alpinelinux.org/packages?name=bash)

GNU Bourne Again SHell

(Unofficial Demo and Howto)

## Contents
* Applies concepts form https://gitlab.com/hub-docker-com-demo/alpine
  * Using "entrypoint" to change default shell.
  * Adding a user and executing the shell as non-root.
  * Allowing the non-root user to use sudo.
  * Installing (apk) packages before executing the (yaml) script.
  * Timing packages installation and yaml script execution time with "time".
  * See [.gitlab-ci.yml](httpshttps://gitlab.com/alpinelinux-packages-demo/bash/blob/master/.gitlab-ci.yml) file.


## License
* http://tiswww.case.edu/php/chet/bash/bashtop.html

## See also
* https://tracker.debian.org/pkg/bash

## Documentation used
* https://unix.stackexchange.com/questions/26676/how-to-check-if-a-shell-is-login-interactive-batch